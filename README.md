# AAP Diagnosis Android Application

Android application client to run on and communicate with the base application.

## Prerequisites

* Android Studio - Available from https://developer.android.com/studio
* Virtualisation enabled on your machine/an android device to run the application on

## Running the Application

The application will need to be opened and run in android studio
The application will need to be configured in android studio,
e.g. the project is defined and android studio knows how to find and run the app

Note: if using the emulator the phone model, OS and API will need to be defined in
android studio.

The Retrofit BASE_URL variable will need to be set to the APIs URL.

Once this is all done, run the base application (follow the README instructions)
prior to running the mobile application to allow for API calls.

*****************************************************************
Warning - Android studio is very resource intensive
It requires a lot of storage, memory and processing power to run
please allow for the adequate system resources to be available
*****************************************************************
